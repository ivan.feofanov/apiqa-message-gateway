module go-message-gateway

require (
	cloud.google.com/go v0.30.0
	firebase.google.com/go v3.7.0+incompatible
	github.com/DATA-DOG/go-txdb v0.1.2
	github.com/Ivan-Feofanov/go-helpers v0.0.0-20190321063755-8f2fad8c8669
	github.com/alexsasharegan/dotenv v0.0.0-20171113213728-090a4d1b5d42
	github.com/corpix/uarand v0.0.0 // indirect
	github.com/gin-contrib/sse v0.0.0-20170109093832-22d885f9ecc7 // indirect
	github.com/gin-gonic/gin v0.0.0-20170702092826-d459835d2b07
	github.com/go-ini/ini v1.46.0 // indirect
	github.com/go-redis/redis v6.13.2+incompatible
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/google/martian v2.1.0+incompatible // indirect
	github.com/googleapis/gax-go v2.0.0+incompatible // indirect
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/jarcoal/httpmock v1.0.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/mailgun/mailgun-go/v3 v3.3.3
	github.com/mattn/go-isatty v0.0.3 // indirect
	github.com/minio/minio-go v6.0.14+incompatible
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/ugorji/go v1.1.1 // indirect
	go.opencensus.io v0.17.0 // indirect
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
	golang.org/x/oauth2 v0.0.0-20181017192945-9dcd33a902f4 // indirect
	google.golang.org/api v0.0.0-20181021000519-a2651947f503
	google.golang.org/appengine v1.2.0 // indirect
	google.golang.org/genproto v0.0.0-20181016170114-94acd270e44e // indirect
	google.golang.org/grpc v1.15.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/ini.v1 v1.46.0 // indirect
)

go 1.13
