package main

import (
	"go-message-gateway/config"
	"go-message-gateway/handlers"
	"log"

	gh "github.com/Ivan-Feofanov/go-helpers"
)

func main() {
	env := config.GetInstance()
	defer gh.Close(env.Db)

	r := handlers.SetupRouter(env)
	err := r.Run() // listen and serve on 0.0.0.0:8080
	if err != nil {
		log.Fatal(err)
	}
}
