package testutils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
)

type BodyCreator interface {
	Create(values map[string]interface{}) (*bytes.Buffer, string)
}

type FormDataBodyCreator struct{}

func (f FormDataBodyCreator) Create(values map[string]interface{}) (*bytes.Buffer, string) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	for key, value := range values {
		v := fmt.Sprint(value)
		_ = writer.WriteField(key, v)
	}
	err := writer.Close()
	if err != nil {
		panic(err)
	}
	return body, writer.FormDataContentType()
}

type JSONBodyCreator struct{}

func (j JSONBodyCreator) Create(values map[string]interface{}) (*bytes.Buffer, string) {
	JSONData, err := json.Marshal(values)
	if err != nil {
		panic(err)
	}
	return bytes.NewBuffer(JSONData), "application/json;"
}

func PerformRequest(
	r http.Handler, method, path, login, password, contentType string,
	body io.Reader) *httptest.ResponseRecorder {

	req, _ := http.NewRequest(method, path, body)
	req.SetBasicAuth(login, password)
	req.Header.Add("Content-Type", contentType)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}
