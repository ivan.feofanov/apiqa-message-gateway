CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS emails (
  uid uuid not null primary key default uuid_generate_v4(),
  created_at timestamp without time zone default (now() at time zone 'utc'),
  updated_at timestamp without time zone default (now() at time zone 'utc'),
  sender varchar(255) not null,
  recipients varchar(255)[] not null,
  subject varchar(255) not null,
  text text not null,
  status int not null default 0,
  error_message varchar(255)
);

CREATE TABLE IF NOT EXISTS attachments (
  uid uuid not null primary key default uuid_generate_v4(),
  created_at timestamp without time zone default (now() at time zone 'utc'),
  updated_at timestamp without time zone default (now() at time zone 'utc'),
  path varchar(255) not null,
  email_id uuid not null references emails(uid) on delete cascade
);

CREATE TABLE IF NOT EXISTS  sms (
  uid uuid not null primary key default uuid_generate_v4(),
  created_at timestamp without time zone default (now() at time zone 'utc'),
  updated_at timestamp without time zone default (now() at time zone 'utc'),
  phone varchar(20) not null,
  text varchar(255) not null,
  status int not null default 0,
  error_message varchar(255)
);