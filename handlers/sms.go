package handlers

import (
	"encoding/json"
	"go-message-gateway/config"
	"log"
	"net/http"
	"net/url"
	"regexp"
)

type SMS struct {
	Phone string `form:"phone" json:"phone" binding:"required"`
	Text  string `form:"text" json:"text" binding:"required"`
}

type Response struct {
	Error     string `json:"error"`
	ErrorCode int    `json:"error_code" validation:"required"`
	ID        int    `json:"id"`
	Count     int    `json:"cnt"`
}

func SMSHandler(sms SMS, env *config.Env) {
	db := env.Db
	cfg := env.Gateway.SMS

	reg := regexp.MustCompile(`\D`)
	phone := reg.ReplaceAllString(sms.Phone, "")

	baseURL, err := url.Parse(cfg.URL)
	if err != nil {
		log.Fatal(err)
	}
	params := url.Values{}
	params.Add("login", cfg.Login)
	params.Add("psw", cfg.Password)
	params.Add("sender", cfg.Sender)
	params.Add("phones", phone)
	params.Add("mes", sms.Text)
	params.Add("fmt", "3") // "3" means json response format. Docs: https://smsc.ru/api/
	baseURL.RawQuery = params.Encode()

	resp, err := http.Get(baseURL.String())
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	rows := db.QueryRowx(`
		INSERT INTO sms (phone, text, status) values ($1, $2, $3) RETURNING uid
	`, sms.Phone, sms.Text, config.Created)

	var uid string
	err = rows.Scan(&uid)
	if err != nil {
		log.Fatal(err)
	}

	var r Response
	err = json.NewDecoder(resp.Body).Decode(&r)
	if err != nil {
		log.Fatal(err)
	}

	var status int
	var errorMessage string

	if r.ErrorCode != 0 {
		status = config.Failed
		errorMessage = r.Error
	} else {
		status = config.Success
	}
	db.MustExec(`UPDATE sms SET status = $1, error_message = $2 WHERE uid = $3`, status, errorMessage, uid)

}
