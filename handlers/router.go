package handlers

import (
	"fmt"
	"go-message-gateway/config"
	"net/http"

	"firebase.google.com/go/messaging"
	"github.com/gin-gonic/gin"
)

func SetupRouter(env *config.Env) *gin.Engine {
	r := gin.Default()

	authorized := r.Group("/", gin.BasicAuth(gin.Accounts{env.Auth.Username: env.Auth.Password}))

	authorized.POST("/:transport", func(context *gin.Context) {
		workDone := make(chan bool)
		var result string
		switch transport := context.Param("transport"); transport {
		case "email":
			var emailJSON Email

			if err := context.Bind(&emailJSON); err != nil {
				context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			go EmailHandler(context, emailJSON, workDone, env)
			<-workDone
			result = fmt.Sprintf("%s queued", transport)
		case "sms":
			var sms SMS
			if err := context.Bind(&sms); err != nil {
				context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			go SMSHandler(sms, env)
			result = fmt.Sprintf("%s queued", transport)
		case "fcm":
			var pushMessage messaging.Message
			if err := context.Bind(&pushMessage); err != nil {
				context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			go FCMHandler(&pushMessage, env)
			result = fmt.Sprintf("%s queued", transport)
		default:
			context.String(400, "wrong transport %s", transport)
			return
		}
		context.JSON(http.StatusOK, gin.H{"status": result})
	})

	return r
}
