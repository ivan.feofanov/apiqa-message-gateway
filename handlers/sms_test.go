package handlers

import (
	"fmt"
	"go-message-gateway/config"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-txdb"
	gh "github.com/Ivan-Feofanov/go-helpers"
	"github.com/jarcoal/httpmock"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func TestSMSHandler(t *testing.T) {
	txdb.Register("sender_test_sms", "postgres", "postgres://postgres@localhost/sender?sslmode=disable")
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	resp := Response{ID: 1, Count: 1, Error: "errr", ErrorCode: 200}
	responder, _ := httpmock.NewJsonResponder(200, resp)
	httpmock.RegisterResponder("GET", "http://localhost", responder)

	var env config.Env
	env.Gateway.SMS.URL = "http://localhost"
	env.Db = sqlx.MustConnect("sender_test_sms", "identifier")
	defer gh.Close(env.Db)

	tests := []struct {
		name    string
		payload SMS
	}{
		{
			name: "Test sms sending",
			payload: SMS{
				Phone: "79000000000",
				Text:  "Test text",
			},
		},
		{
			name: "Test bad number",
			payload: SMS{
				Phone: "+7 (902) 583-32-53",
				Text:  "Test text",
			},
		},
	}

	for _, tt := range tests {
		step := tt
		t.Run(tt.name, func(t *testing.T) {
			SMSHandler(step.payload, &env)

			var sms SMS
			err := env.Db.QueryRowx("SELECT phone, text from sms").StructScan(&sms)
			if err != nil {
				t.Fatalf("Queryng error: %d", err)
			}

			reg := regexp.MustCompile(`\D`)
			phone := reg.ReplaceAllString(sms.Phone, "")
			fmt.Print(sms)
			if phone != sms.Phone {
				t.Fatalf("Wrong phone: %d", err)
			}
			if step.payload.Text != sms.Text {
				t.Fatalf("Wrong text: %d", err)
			}
		})
	}
}
