package handlers

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"go-message-gateway/config"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"

	"github.com/jmoiron/sqlx"
	"github.com/minio/minio-go"

	gh "github.com/Ivan-Feofanov/go-helpers"
	"github.com/mailgun/mailgun-go/v3"

	"github.com/gin-gonic/gin"
	"github.com/lib/pq"
)

type Attachment struct {
	Title string `json:"title" binding:"required"`
	Data  string `json:"data" binding:"required"`
}

type Email struct {
	From        string       `form:"from" json:"from" binding:"min=1,required,email"`
	To          []string     `form:"to" json:"to" binding:"dive,email"`
	Text        string       `form:"text" json:"text" binding:"required"`
	Subject     string       `form:"subject" json:"subject" binding:"required"`
	Attachments []Attachment `form:"attachments" json:"attachments"`
}

// TODO: move to different branch
//func cloudUpload(fileName string, fileData []byte, public bool, env *config.Env) string {
//	ctx := context.Background()
//	cfg := env.GCloud
//	client, err := storage.NewClient(ctx, option.WithCredentialsJSON(cfg.Credentials))
//	if err != nil {
//		log.Fatal(err)
//	}
//	bucket := client.Bucket(cfg.BucketName)
//	if _, err = bucket.Attrs(ctx); err != nil {
//		log.Fatal(err)
//	}
//
//	obj := bucket.Object(fileName)
//	w := obj.NewWriter(ctx)
//
//	if _, err := io.Copy(w, bytes.NewReader(fileData)); err != nil {
//		log.Fatal(err)
//	}
//
//	if err := w.Close(); err != nil {
//		log.Fatal(err)
//	}
//
//	if public {
//		if err := obj.ACL().Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
//			log.Fatal(err)
//		}
//	}
//
//	attrs, err := obj.Attrs(ctx)
//	if err != nil {
//		log.Fatal(err)
//	}
//	return fmt.Sprintf("https://storage.googleapis.com/%s/%s", attrs.Bucket, attrs.Name)
//}

func minioUpload(fileName string, fileData []byte) string {
	hostname := config.GetInstance().Minio.Endpoint
	client := config.GetInstance().Minio.Client
	bucket := config.GetInstance().Minio.Bucket

	_, err := client.PutObject(bucket, fileName, bytes.NewReader(fileData), int64(len(fileData)), minio.PutObjectOptions{})
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("http://%s/%s/%s", hostname, bucket, fileName)
}

func addAttachments(tx *sqlx.Tx, message *mailgun.Message, files map[string][]byte, uid string) error {
	stmt, err := tx.Preparex(pq.CopyIn("attachments", "path", "email_id"))
	if err != nil {
		return err
	}

	for fileName, fileData := range files {
		filePath := minioUpload(fileName, fileData)
		stmt.MustExec(filePath, uid)
		message.AddReaderAttachment(fileName, ioutil.NopCloser(bytes.NewReader(fileData)))
	}
	stmt.MustExec()
	gh.Close(stmt)
	return nil
}

func getFilesFromMultipart(form *multipart.Form, files map[string][]byte) {
	for _, file := range form.File["attachments"] {
		fileContent, err := file.Open()
		if err != nil {
			log.Fatal(err)
		}
		buf := bytes.NewBuffer(nil)
		_, err = io.Copy(buf, fileContent)
		if err != nil {
			log.Fatal(err)
		}
		files[file.Filename] = buf.Bytes()
	}
}

func EmailHandler(context *gin.Context, emailJSON Email, workDone chan bool, env *config.Env) {
	db := env.Db
	mgClient := env.MailgunClient

	cfg := env.Gateway.Mailgun
	mg := mailgun.NewMailgun(cfg.Domain, cfg.APIKey)
	message := mg.NewMessage("noreply@pik-comfort.ru", emailJSON.Subject, emailJSON.Text, emailJSON.To...)

	tx := db.MustBegin()
	rows := tx.QueryRowx(`
		INSERT INTO emails (sender, recipients, subject, text) values ($1, $2, $3, $4) RETURNING uid
	`, emailJSON.From, pq.Array(emailJSON.To), emailJSON.Subject, emailJSON.Text)

	files := map[string][]byte{}

	for _, file := range emailJSON.Attachments {
		fileBuffer, _ := base64.StdEncoding.DecodeString(file.Data)
		files[file.Title] = fileBuffer
	}

	if form, err := context.MultipartForm(); err == nil {
		getFilesFromMultipart(form, files)
	}

	workDone <- true

	var uid string
	err := rows.Scan(&uid)
	if err != nil {
		log.Fatal(err)
	}

	if len(files) > 0 {
		err := addAttachments(tx, message, files, uid)
		if err != nil {
			log.Fatal(err)
		}
	}

	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
	_, _, err = mgClient.Send(context, message, mg)

	var status int
	var errorMessage string

	if err != nil {
		status = config.Failed
		errorMessage = err.Error()
	} else {
		status = config.Success
	}
	db.MustExec(`UPDATE emails SET status = $1, error_message = $2 WHERE uid = $3`, status, errorMessage, uid)
}
