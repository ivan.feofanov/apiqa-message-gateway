package handlers

import (
	"context"
	"encoding/json"
	"go-message-gateway/config"
	testutils "go-message-gateway/test_utils"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/alexsasharegan/dotenv"

	"github.com/DATA-DOG/go-txdb"
	gh "github.com/Ivan-Feofanov/go-helpers"
	"github.com/icrowley/fake"
	"github.com/jmoiron/sqlx"
	"github.com/mailgun/mailgun-go/v3"
	"github.com/stretchr/testify/assert"
)

type MailgunMock struct{}

func (mm MailgunMock) Send(ctx context.Context, message *mailgun.Message, m *mailgun.MailgunImpl) (mes string, id string, err error) {
	return "", "", nil
}
func TestMain(m *testing.M) {
	err := dotenv.Load("../.env")
	if err != nil {
		log.Printf("Error loading .env file: %v", err)
	}
	dbOptions := config.MakeDBOptions()

	db, err := sqlx.Connect("postgres", dbOptions)
	if err != nil {
		log.Fatal(err)
	}
	schema, err := ioutil.ReadFile("../database/schema.sql")
	if err != nil {
		log.Fatal(err)
	}

	db.MustExec(string(schema))
	code := m.Run()
	os.Exit(code)
}
func TestEmailHandler(t *testing.T) {
	dbOptions := config.MakeDBOptions()
	txdb.Register("sender_test_email", "postgres", dbOptions)

	var env config.Env

	env.Db = sqlx.MustConnect("sender_test_email", "identifier")
	defer gh.Close(env.Db)

	env.MailgunClient = MailgunMock{}

	env.Auth.Username = "admin"
	env.Auth.Password = "111"

	r := SetupRouter(&env)

	tests := []struct {
		name        string
		bodyCreator testutils.BodyCreator
		payload     map[string]interface{}
	}{
		{
			name:        "Test receive FormData email request",
			bodyCreator: testutils.FormDataBodyCreator{},
			payload: map[string]interface{}{
				"from":    fake.EmailAddress(),
				"to":      fake.EmailAddress(),
				"subject": fake.EmailSubject(),
				"text":    fake.Paragraphs(),
			},
		},
		{
			name:        "Test receive JSON email request",
			bodyCreator: testutils.JSONBodyCreator{},
			payload: map[string]interface{}{
				"from":    fake.EmailAddress(),
				"to":      []string{fake.EmailAddress()},
				"subject": fake.EmailSubject(),
				"text":    fake.Paragraphs(),
			},
		},
	}
	for _, tt := range tests {
		step := tt
		t.Run(step.name, func(t *testing.T) {
			body, contentType := step.bodyCreator.Create(step.payload)
			w := testutils.PerformRequest(r, "POST", "/email", env.Auth.Username, env.Auth.Password, contentType, body)
			assert.Equal(t, http.StatusOK, w.Code)

			var resp struct {
				Status string `json:"status"`
			}
			err := json.NewDecoder(w.Body).Decode(&resp)
			if err != nil {
				panic(err)
			}
			assert.Equal(t, resp.Status, "email queued")
		})
	}
}
