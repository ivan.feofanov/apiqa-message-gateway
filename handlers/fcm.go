package handlers

import (
	"fmt"
	"go-message-gateway/config"
	"log"

	"firebase.google.com/go/messaging"

	"golang.org/x/net/context"
)

func FCMHandler(msg *messaging.Message, env *config.Env) {
	ctx := context.Background()
	client, _ := env.FirebaseApp.Messaging(ctx)

	// Send a message to the device corresponding to the provided
	// registration token.

	response, err := client.Send(ctx, msg)
	if err != nil {
		log.Fatalln(err)
	}
	// Response is a message ID string.
	fmt.Println("Successfully sent message:", response)
}
