package config

import (
	"context"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"sync"

	firebase "firebase.google.com/go"
	"github.com/mailgun/mailgun-go/v3"
	"github.com/minio/minio-go"
	"google.golang.org/api/option"

	"github.com/alexsasharegan/dotenv"
	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
)

const (
	Created int = iota
	Success
	Failed
)

type MailgunClient interface {
	Send(ctx context.Context, message *mailgun.Message, m *mailgun.MailgunImpl) (mes string, id string, err error)
}

type Mailgun struct {
}

func (mg Mailgun) Send(ctx context.Context, message *mailgun.Message, m *mailgun.MailgunImpl) (mes string, id string, err error) {
	return m.Send(ctx, message)
}

//Env синглтон с необходимой обвязкой для получения данных из db и через api
type Env struct {
	IsProduction  bool
	MailgunClient MailgunClient
	Db            *sqlx.DB
	Redis         *redis.Client
	Gateway       struct {
		Mailgun struct {
			Domain       string
			APIKey       string
			PublicAPIKey string
		}
		SMS struct {
			URL      string
			Login    string
			Password string
			Sender   string
		}
	}
	GCloud struct {
		ProjectID   string
		BucketName  string
		Credentials []byte
	}
	Minio struct {
		Client   *minio.Client
		Bucket   string
		Endpoint string
	}
	FCM struct {
		ProjectID   string
		Credentials string
	}
	FirebaseApp *firebase.App
	Auth        struct {
		Username string
		Password string
	}
}

func setupMinio(env *Env) error {
	env.Minio.Endpoint = os.Getenv("MINIO_ENDPOINT")
	bucketName := os.Getenv("MINIO_BUCKET")
	accessKeyID := os.Getenv("MINIO_ACCESS_KEY_ID")
	secretAccessKey := os.Getenv("MINIO_SECRET_ACCESS_KEY")
	useSSL, err := strconv.ParseBool(os.Getenv("MINIO_USE_SSL"))
	if err != nil {
		return err
	}

	minioClient, err := minio.New(env.Minio.Endpoint, accessKeyID, secretAccessKey, useSSL)
	if err != nil {
		return err
	}
	err = minioClient.MakeBucket(bucketName, "")
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, err := minioClient.BucketExists(bucketName)
		if err == nil && exists {
			log.Printf("We already own %s\n", bucketName)
		} else {
			log.Fatal(err)
		}
	}

	policy := `{
		"Version":"2012-10-17",
		"Statement":[{
			"Effect":"Allow",
			"Principal":{"AWS":["*"]},
			"Action":["s3:GetObject"],
			"Resource":["arn:aws:s3:::sender/*"]}]
	}`
	err = minioClient.SetBucketPolicy(bucketName, policy)
	if err != nil {
		return err
	}

	env.Minio.Client = minioClient
	env.Minio.Bucket = bucketName
	return nil
}

func setupFirebase(env *Env) {
	conf := &firebase.Config{ProjectID: os.Getenv("FCM_PROJECT_ID")}

	c, err := base64.StdEncoding.DecodeString(os.Getenv("FCM_CREDENTIALS"))
	if err != nil {
		panic(err)
	}
	opt := option.WithCredentialsJSON(c)

	env.FirebaseApp, err = firebase.NewApp(context.Background(), conf, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}
}

func MakeDBOptions() string {
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbName := os.Getenv("DB_NAME")
	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")

	return fmt.Sprintf(
		"host=%s port=%s dbname=%s user=%s password=%s sslmode=disable", dbHost, dbPort, dbName, dbUsername, dbPassword)
}

func setupDatabase(env *Env) (err error) {
	dbOptions := MakeDBOptions()
	fmt.Print(dbOptions)
	env.Db, err = sqlx.Connect("postgres", dbOptions)
	if err != nil {
		return err
	}

	schema, err := ioutil.ReadFile("database/schema.sql")
	if err != nil {
		return err
	}

	env.Db.MustExec(string(schema))
	return nil
}

func (env *Env) setup() {
	err := dotenv.Load()
	if err != nil {
		if !env.IsProduction {
			log.Fatalf("Error loading .env file: %v", err)
		}
	}

	if os.Getenv("ENVIRONMENT") == "production" {
		env.IsProduction = true
	} else {
		env.IsProduction = false
	}

	err = setupDatabase(env)
	if err != nil {
		log.Fatal(err)
	}

	env.Redis = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	env.Gateway.Mailgun.Domain = os.Getenv("MAILGUN_DOMAIN")
	env.Gateway.Mailgun.APIKey = os.Getenv("MAILGUN_API_KEY")
	env.Gateway.Mailgun.PublicAPIKey = os.Getenv("MAILGUN_PUBLIC_API_KEY")

	env.Gateway.SMS.URL = os.Getenv("SMS_URL")
	env.Gateway.SMS.Sender = os.Getenv("SMS_SENDER")
	env.Gateway.SMS.Login = os.Getenv("SMS_LOGIN")
	env.Gateway.SMS.Password = os.Getenv("SMS_PASSWORD")

	env.GCloud.ProjectID = os.Getenv("PROJECT_ID")
	env.GCloud.BucketName = os.Getenv("BUCKET_NAME")
	env.GCloud.Credentials, err = base64.StdEncoding.DecodeString(os.Getenv("CREDENTIALS"))
	if err != nil {
		log.Fatal(err)
	}

	err = setupMinio(env)
	if err != nil {
		log.Fatal(err)
	}

	env.MailgunClient = Mailgun{}

	// Firebase APP
	setupFirebase(env)

	// Client username and password
	env.Auth.Username = os.Getenv("USERNAME")
	env.Auth.Password = os.Getenv("PASSWORD")
}

// GetInstance получить текущий обьект Env
func GetInstance() *Env {
	var instance *Env
	var once sync.Once

	once.Do(func() {
		instance = &Env{}
		instance.setup()
	})
	return instance
}
